import React, { Component } from "react";
import { View, Text, StyleSheet, Dimensions, Image, TouchableWithoutFeedback } from "react-native";
import { Pages } from "react-native-pages";

const screenWidth = Dimensions.get("window").width;
const ratioHeight = screenWidth * 4 / 12;

import themes from "../Themes";

class Slideslow extends Component {
  constructor(props) {
    super(props);
    this.pageRef = this.pageRef.bind(this);
  }

  //FIXME: Find a better way to controle this component update state
  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.count >= 8) {
      return false;
    }
    return true;
  }

  pageRef(ref) {
    this._pages = ref;
  }

  state = {
    height: this.props.square ? screenWidth : ratioHeight,
    width: screenWidth,
    autoPlay: this.props.autoPlay,
    count: 0,
  };

  componentDidMount() {
    if (this.state.autoPlay) {
      this.interval = setInterval(() => this.moveToNext(), 6000);
    }
  }

  moveToNext() {
    let { images } = this.props;
    let index = this.state.index + 1 < images.length ? (index = this.state.index + 1) : (index = 0);
    if (this._pages) {
      this._pages.scrollToPage(index);
      this.setState({ index: index, count: ++this.state.count });
    }
  }

  _renderScene = () => {
    let { images } = this.props;
    if (images == null || images.length <= 0) return null;
    const render = images.map((image, index) => (
      <TouchableWithoutFeedback key={index} onPress={() => this.props.onPress(image)}>
        <Image style={{ width: screenWidth, height: this.state.height }} resizeMode="cover" source={{ uri: image }} />
      </TouchableWithoutFeedback>
    ));

    return render;
  };

  render() {
    let { height, width } = this.state;
    return (
      <View style={[styles.container, { height, width }]}>
        <Pages ref={this.pageRef}>{this._renderScene()}</Pages>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: themes.ef,
  },
  indicatorContainer: {
    flexDirection: "row",
    width: screenWidth,
    height: 30,
    position: "absolute",
    bottom: 0,
    justifyContent: "center",
    alignItems: "center",
  },
  inactiveIndicator: {
    backgroundColor: "#FFF",
    height: 8,
    width: 8,
    borderRadius: 4,
    marginHorizontal: 2,
  },
  activeIndicator: {
    backgroundColor: themes.defaultPrimaryColor,
    height: 8,
    width: 8,
    borderRadius: 4,
    marginHorizontal: 2,
  },
});

export default Slideslow;
