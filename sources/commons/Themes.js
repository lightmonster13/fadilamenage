exports.darkPrimaryColor = "#D63500";
exports.defaultPrimaryColor = "#FB5C28";
exports.lightPrimaryColor = "#FC845E";
exports.accentColor = "#F2F5FF";
exports.primaryTextColor = "#FFFFFF";
exports.secondaryTextColor = "#F2F2F2";
exports.dividerColor = "#198576";
exports.whiteSmoke = "#F5F5F5";

exports.appBackgroundColor = "#F5F5F5";
exports.ef = "#EFEFEF";
exports.dd = "#DDDDDD";
exports.rougeCoeur = "#ED2020";

exports.colorTansparent = "rgba(0, 0, 0, 0.40)";

exports.h0 = 12;
exports.h1 = 14;
exports.h2 = 16;
exports.h3 = 18;
exports.h4 = 20;
exports.h5 = 22;
exports.h6 = 24;
exports.h7 = 26;
exports.h8 = 28;
exports.h9 = 30;
exports.h10 = 32;

exports.m2 = 2;
exports.m4 = 4;
exports.m5 = 5;

exports.m6 = 6;
exports.m8 = 8;
exports.m10 = 10;
exports.m12 = 12;
exports.m14 = 14;
exports.m16 = 16;
exports.m18 = 18;
exports.m20 = 20;
exports.m32 = 32;
