import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableHighlight } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

import Themes from "./Themes";

export default class EditBtn extends Component {
  navigateToEditProfil() {
    let { navigate } = this.props.navigation;
    navigate("editProfil", {});
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={{ justifyContent: "center" }}>
          <TouchableHighlight
            style={[styles.icoContainer]}
            underlayColor="#EFEFEF"
            onPress={() => this.navigateToEditProfil()}>
            <Icon name="md-create" size={24} color={Themes.dividerColor} />
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {},
  icoContainer: {
    alignItems: "center",
    justifyContent: "center",
    paddingRight: Themes.m16,
  },
});
