import React from "react";
import { Platform } from "react-native";
import { createAppContainer, createStackNavigator, TabNavigator, createBottomTabNavigator } from "react-navigation";
import Ionicons from "react-native-vector-icons/Ionicons";
import Icon from "react-native-vector-icons/Ionicons";

import Themes from "../commons//Themes";

import Onboard from "../screens/home/Onboard";
import Login from "../screens/home/Login";

// ROUTES PRINCIPALES
import Section from "../screens/section/Section";
import Boutique from "../screens/boutique/boutique";
import Blog from "../screens/blog/Blog";
//import Profile from "../screens/profil/Profile";
import { Details } from "../screens/boutique/index";
import Profil from "../screens/profil/Profil";

//SUB_ROUTES
import Entretien from "../screens/entretien/Entretien";
import ClientProfil from "../screens/client/ClientProfil";
import Certifier from "../screens/certifier/Certifier";
import MonProfil from "../screens/profil/MonProfil";
import EditProfil from "../screens/editProfil/EditProfil";
import Nounous from "../screens/nounous/Nounous";
import Chef from "../screens/chef/Chef";

let SUB_ROUTES = {
  entretien: { screen: Entretien },
  details: { screen: Details },
  clientProfil: { screen: ClientProfil },
  certifier: { screen: Certifier },
  monProfil: { screen: MonProfil },
  editProfil: { screen: EditProfil },
  nounous: { screen: Nounous },
  chef: { screen: Chef },
};

const TABS = createBottomTabNavigator(
  {
    Section: {
      screen: createStackNavigator({ Section, ...SUB_ROUTES }),
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="md-list" size={24} color={tintColor} />,
        tabBarStyle: {
          height: 0,
        },
        headerStyle: {
          height: 0,
        },
      },
    },
    Boutique: {
      screen: createStackNavigator({ Boutique, ...SUB_ROUTES }),
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="md-basket" size={24} color={tintColor} />,
        tabBarStyle: {
          height: 0,
        },
        headerStyle: {
          height: 0,
        },
      },
    },
    Blog: {
      screen: createStackNavigator({ Blog, ...SUB_ROUTES }),
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="md-apps" size={24} color={tintColor} />,
        tabBarStyle: {
          height: 0,
        },
        headerStyle: {
          height: 0,
        },
      },
    },
    Profil: {
      screen: createStackNavigator({ Profil, ...SUB_ROUTES }),
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="md-contact" size={24} color={tintColor} />,
        tabBarStyle: {
          height: 0,
        },
        headerStyle: {
          height: 0,
        },
      },
    },
  },
  {
    initialRouteName: "Section",

    swipeEnabled: Platform.OS === "ios" ? false : false,
    animationEnabled: false,
    tabBarOptions: {
      showIcon: true,
      showLabel: true,
      // upperCaseLabel: false,
      activeTintColor: "#FFF",
      inactiveTintColor: Themes.colorTansparentd,
      indicatorStyle: {
        backgroundColor: Themes.defaultPrimaryColor,
      },
      labelStyle: {
        fontSize: 12,
        marginVertical: 0,
      },
      style: {
        backgroundColor: Themes.defaultPrimaryColor,
      },
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => getTabBarIcon(navigation, focused, tintColor),
    }),

    tabBarOptions: {
      activeTintColor: "tomato",
      inactiveTintColor: "white",
      showIcon: true,
      indicatorStyle: {
        backgroundColor: Themes.lightPrimaryColor,
      },
      labelStyle: {
        fontSize: 14,
        marginVertical: 0,
      },
      style: {
        backgroundColor: Themes.defaultPrimaryColor,
      },
    },
  },
);

const ROUTES = createStackNavigator(
  {
    onboard: { screen: Onboard },
    login: { screen: Login },
    tabs: { screen: TABS },
  },
  { headerMode: "none" },
);

export default createAppContainer(ROUTES);
