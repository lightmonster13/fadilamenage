import React, { Component } from "react";
import { View, Text, StyleSheet, Image } from "react-native";

import Themes from "../../commons/Themes";

import Onboarding from "react-native-onboarding-swiper";

export default class componentName extends Component {
  static navigationOptions = {
    headerStyle: {
      height: null,
    },
  };

  navigateToLogin() {
    let { navigate } = this.props.navigation;
    navigate("login", {});
  }
  render() {
    return (
      <Onboarding
        nextLabel={"Suivant"}
        skipLabel={"Passer"}
        onSkip={() => this.navigateToLogin()}
        onDone={() => this.navigateToLogin()}
        pages={[
          {
            backgroundColor: Themes.defaultPrimaryColor,
            image: (
              <Image
                style={{
                  height: 200,
                  width: 200,
                }}
                source={require("../../commons/assets/Satisfaction.png")}
              />
            ),
            title: "Satisfaction",
            subtitle:
              "Ayez à votre disposition des personnes fiables aimables ponctuelles certifiées pour la prise en charge de votre Château.",
          },
          {
            backgroundColor: Themes.dividerColor,
            image: (
              <Image
                style={{
                  height: 200,
                  width: 200,
                }}
                source={require("../../commons/assets/shop.png")}
              />
            ),
            title: "Shop",
            subtitle: "Procurez-vous des produits et articles de bonnes qualités au meilleur prix.",
          },
          {
            backgroundColor: Themes.primaryTextColor,
            image: (
              <Image
                style={{
                  height: 200,
                  width: 200,
                }}
                source={require("../../commons/assets/formation1.png")}
              />
            ),
            title: "Formation",
            subtitle: "Faites-vous certifier pour appartenir à notre communauté. ",
          },
        ]}
      />
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
