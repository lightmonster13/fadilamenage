import React, { Component } from "react";
import { View, Text, Image, StyleSheet, TextInput, TouchableHighlight, ScrollView } from "react-native";

import themes from "../../commons/Themes";
import Icon from "react-native-vector-icons/Ionicons";
//ort Icon from "react-native-ionicons";

export default class Login extends Component {
  static navigationOptions = {
    headerStyle: {
      height: null,
    },
  };

  navigateToProfil() {
    let { navigate } = this.props.navigation;
    navigate("Profil", {});
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={{ backgroundColor: themes.whiteSmoke }}>
          <View style={styles.container}>
            <View
              style={{
                height: 150,
                width: 150,
                margin: themes.m32,
                alignSelf: "center",
                justifyContent: "center",
                alignItems: "center",
              }}>
              <Image
                style={{
                  height: 150,
                  width: 150,
                }}
                source={require("../../commons/assets/fadilamenage.png")}
              />
            </View>
            <View style={{ flex: 1 }}>
              <View style={{ alignItems: "center", marginHorizontal: themes.m32 }}>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    borderBottomWidth: 0.5,
                    borderColor: themes.dividerColor,
                  }}>
                  <Icon name="md-person" size={24} color={themes.dividerColor} />
                  <TextInput
                    underlineColorAndroid={"transparent"}
                    keyboardType={"numeric"}
                    placeholder={"Numéro de téléphone"}
                    // onChangeText={str => {
                    //   this.setState({ codeScurite: str });
                    //   G_codeScurite = str;
                    // }}
                    placeholderTextColor={"#ccc"}
                    // onFocus={locals.onFocus}
                    // autoFocus={locals.autoFocus}

                    style={{
                      paddingLeft: 16,
                      fontSize: 18,
                      flex: 1,
                    }}
                  />
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    borderBottomWidth: 0.5,
                    borderColor: themes.dividerColor,
                  }}>
                  <Icon name="md-lock" size={29} color={themes.dividerColor} />
                  <TextInput
                    underlineColorAndroid={"transparent"}
                    keyboardType={"numeric"}
                    secureTextEntry={true}
                    placeholder={"Code"}
                    // onChangeText={str => {
                    //   this.setState({ codeScurite: str });
                    //   G_codeScurite = str;
                    // }}
                    placeholderTextColor={"#ccc"}
                    style={{
                      paddingLeft: 16,
                      fontSize: 18,
                      flex: 1,
                    }}
                  />
                </View>
              </View>
              <View style={styles.btnCont}>
                <TouchableHighlight
                  style={{ alignItems: "center", alignSelf: "center" }}
                  underlayColor={themes.dd}
                  onPress={() => this.navigateToProfil()}>
                  <View style={styles.buyBtn}>
                    <Text style={{ color: themes.primaryTextColor, paddingLeft: 3, paddingRight: 3, fontSize: 18 }}>
                      CONNECTER
                    </Text>
                  </View>
                </TouchableHighlight>
                <View>
                  <Text style={{ color: themes.dividerColor, fontSize: 14, marginTop: themes.m16 }} onPress={() => {}}>
                    Ignorer
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
        <View
          style={{
            paddingHorizontal: 32,
            paddingVertical: 12,
            backgroundColor: themes.whiteSmoke,
          }}>
          <Text style={{ fontStyle: "italic", alignSelf: "center" }}>
            En utilisant cette application, vous acceptez les
            <Text
              onPress={() => this.gotoTos()}
              style={{
                textDecorationLine: "underline",
                color: themes.defaultPrimaryColor,
              }}>
              {" "}
              conditions d'utilisations
            </Text>
          </Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: themes.whiteSmoke,
  },
  btnCont: {
    width: 300,
    marginTop: 32,
    alignSelf: "center",
  },
  buyBtn: {
    padding: 4,
    width: 300,
    height: 45,
    minWidth: 125,
    borderRadius: 2,
    alignItems: "center",
    backgroundColor: themes.defaultPrimaryColor,
    justifyContent: "center",
    elevation: 2,
  },
});
