import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  ScrollView,
  Image,
  Linking,
  Share,
  Dimensions,
} from "react-native";

import themes from "../../commons/Themes";
import Slideslow from "../../commons/Slideslow/Slideslow";

const SCREEN_WIDTH = Dimensions.get("window").width;






class Details extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: "Details",
  });
  state = {
    dataSource: [],
  };

  

  render() {
    let {img, desc, prix} = this.props.navigation.state.params;
    this.state.dataSource.push(img)
    return (
      <View>
        <ScrollView style={{ backgroundColor: themes.appBackgroundColor }} showsVerticalScrollIndicator={false}>
       
        {/* {this.renderSlideslow()} */}
        <View style={{ height: SCREEN_WIDTH + 28 }}>
          <Slideslow
            square
            //autoPlay
            images={this.state.dataSource}
            onPress={() => {}}
          />
          </View>

        <View style={styles.footer}>
        <Text style={styles.price}>
          {prix} 
        </Text>

          <View style={styles.descContainer}>
            <Text style={styles.descTxt}>{desc}</Text>
          </View>
        </View>
      </ScrollView>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    paddingHorizontal: themes.m10,
    justifyContent: "center",
    alignItems: "center",
  },
  circle: {
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  imgHolder: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: themes.ef,
    marginRight: themes.m4,
  },
  info: {
    height: 64,
    flexGrow: 1,
    margin: 2,
    justifyContent: "center",
  },
  imageCell: {
    backgroundColor: themes.dd,
    borderRadius: themes.m4,
    height: 320,
    marginHorizontal: themes.m10,
    marginTop: themes.m4,
  },
  shareBtn: {
    height: 44,
    minWidth: 44,
    paddingHorizontal: themes.m6,
    backgroundColor: themes.defaultPrimaryColor,
    borderRadius: 22,
    alignItems: "center",
    justifyContent: "center",
  },
  likeBtn: {
    height: 38,
    minWidth: 38,
    paddingHorizontal: themes.m6,
    backgroundColor: "white",
    borderRadius: 19,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: themes.defaultPrimaryColor,
    marginHorizontal: themes.m10,
  },
  call: {
    backgroundColor: themes.ef,
    borderRadius: 22,
    alignItems: "center",
    justifyContent: "center",
    height: 44,
    width: 44,
    marginHorizontal: themes.m20,
    borderWidth: 1,
    borderColor: themes.defaultPrimaryColor,
    backgroundColor: "white",
  },
  footer: {
    //flexDirection: "row",
    marginHorizontal: themes.m16,
    marginVertical: themes.m10,
    //
  },
  price: {
    color: themes.blue,
    marginTop: 2,
    marginLeft: 8,
    fontSize: 16,
    flexGrow: 1,
  },
  noPrice: {
    color: "grey",
    marginTop: 2,
    marginLeft: 8,
    fontSize: 18,
    flexGrow: 1,
  },
  slideslowHolder: {
    width: SCREEN_WIDTH,
    height: SCREEN_WIDTH,
    backgroundColor: themes.ef,
  },
  btnContainer: {
    flexDirection: "row",
    marginVertical: themes.m8,
    alignItems: "center",
    justifyContent: "space-between",
    width: SCREEN_WIDTH,
    position: "absolute",
    bottom: 0,
  },
  descContainer: {
    marginVertical: themes.m4,
    marginBottom: themes.m16,
    marginLeft: themes.m8,
  },
  descTxt: {
    fontSize: 18,
    color: themes.darkBlue,
  },
});

export default Details;