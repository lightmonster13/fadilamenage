export { default as Details } from "./Details";
export { default as SearchBar } from "./SearchBar";
export { default as BoutiqueItem } from "./BoutiqueItem";
export { default as Boutique } from "./boutique";
