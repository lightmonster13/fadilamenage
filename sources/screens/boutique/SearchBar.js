import React, { Component } from "react";
import { View, Text, TextInput, StyleSheet } from "react-native";
import themes from "../../commons/Themes";

class SearchBar extends Component {
  render() {
    return (
      <View style={styles.searchContainer}>
        <TextInput
          style={styles.iptStyle}
          underlineColorAndroid="transparent"
          disableFullscreenUI={true}
          placeholder="Recherche..."
          onChangeText={str => this.props.onChange(str)}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  searchContainer: {
    backgroundColor: themes.defaultPrimaryColor,
    padding: themes.m16,
  },
  iptStyle: {
    backgroundColor: "white",
    fontSize: themes.h3,
    height: 45,
    borderRadius: 2,
    paddingHorizontal: themes.m10,
  },
});
export default SearchBar;
