import React, { Component } from "react";
import { TouchableHighlight, Image, StyleSheet, Dimensions, View } from "react-native";

import Themes from "../../commons/Themes";

const screenWidth = Dimensions.get("window").width;
const cellWidth = screenWidth / 3 - 4;

class BoutiqueItem extends Component {
  render() {
    return (
      <View style={styles.itemContainer}>
        <TouchableHighlight style={styles.cellContainer} underlayColor={Themes.ef} onPress={() => this.props.onPress()}>
          <Image source={{ uri: this.props.image }} style={[styles.cellContainer, { margin: 0 }]} resizeMode="cover" />
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  itemContainer: {
    width: cellWidth,
    margin: Themes.m2,
  },
  cellContainer: {
    width: cellWidth,
    height: cellWidth,
    backgroundColor: Themes.dd,
  },
  indicator: {
    height: 6,
    width: 6,
    borderRadius: 3,
    alignSelf: "center",
    marginVertical: Themes.m2,
  },
});

export default BoutiqueItem;
