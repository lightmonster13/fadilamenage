//import liraries
import React, { Component } from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";

import Menu, { MenuContext, MenuOptions, MenuOption, MenuTrigger } from "react-native-menu";
import Icon from "react-native-vector-icons/MaterialIcons";

import { BoutiqueItem, SearchBar } from "./index";
import Themes from "../../commons/Themes";

// create a component
class Boutique extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: "Boutique",
  });

  state = {
    value: "Tout",
    dataSource: [
      {
        id: 0,
        desc: "Poudre énergisante des feuilles de PureBodhi Moringa 120g",
        img:
          "https://static.wixstatic.com/media/af1d96_9fac4053362c429e90f902116c80a2db~mv2.jpg/v1/fill/w_576,h_600,al_c,lg_1,q_85/af1d96_9fac4053362c429e90f902116c80a2db~mv2.webp",
        prix: "10.350CFA",
      },
      {
        id: 1,
        desc: "Thé détoxifiant et purifiant de PureBodhi Moringa - 100% Pure Moringa, sans caff",
        img:
          "https://static.wixstatic.com/media/af1d96_36a7d118650e4990ac9c2192cd6a10b6~mv2.jpg/v1/fill/w_600,h_600,al_c,lg_1,q_85/af1d96_36a7d118650e4990ac9c2192cd6a10b6~mv2.webp",
        prix: "8.950CFA",
      },
      {
        id: 2,
        desc: "Moringa a boire mélangé avec le thé vert japonais Matcha de cérémonie - 20",
        img:
          "https://static.wixstatic.com/media/af1d96_8629267b9d89495dbd938b6ff24beda3~mv2_d_3648_3648_s_4_2.png/v1/fill/w_3648,h_3648,al_c,q_85/af1d96_8629267b9d89495dbd938b6ff24beda3~mv2_d_3648_3648_s_4_2.webp",
        prix: "16.550CFA",
      },
      {
        id: 3,
        desc: "Capsules de PureBodhi Moringa 500mg",
        img:
          "https://static.wixstatic.com/media/af1d96_9a8eb50b2e8649eda47facad3c6d1534~mv2_d_1500_1500_s_2.jpg/v1/fill/w_1500,h_1500,al_c,q_85/af1d96_9a8eb50b2e8649eda47facad3c6d1534~mv2_d_1500_1500_s_2.jpg",
        prix: "10.350CFA",
      },
      {
        id: 4,
        desc: "Huile de Moringa pour le visage  – Sérum Anti-Ageing – 100% Pure Moringa – Quali",
        img:
          "https://static.wixstatic.com/media/af1d96_a5279c6d798746d2ac17ef08b1e58221~mv2.jpg/v1/fill/w_900,h_1024,al_c,q_85/af1d96_a5279c6d798746d2ac17ef08b1e58221~mv2.webp",
        prix: "11.700CFA",
      },
      {
        id: 5,
        desc: "Capsules Hairestore de Purebodhi - Pour prévenir la perte de cheveux et ralenti",
        img:
          "https://static.wixstatic.com/media/af1d96_59712cfcdad24678a2f5e5e7aaccd7a0~mv2_d_1500_1500_s_2.jpg/v1/fill/w_1500,h_1500,al_c,q_85/af1d96_59712cfcdad24678a2f5e5e7aaccd7a0~mv2_d_1500_1500_s_2.webp",
        prix: "13.750CFA",
      },
    ],
  };

  navigateToDetails(desc, prix, img) {
    let { navigate } = this.props.navigation;
    navigate("details", { desc, prix, img });
  }
  onSelect(value) {
    switch (value) {
      case "Tout":
        this.setState({ categorie: 0, value: value }, () => this.getExplorer());
        break;
      case "Entretien":
        this.setState({ categorie: 2, value: value }, () => this.getExplorer());
        break;
      case "Bébé":
        this.setState({ categorie: 3, value: value }, () => this.getExplorer());
        break;
      case "Cuisine":
        this.setState({ categorie: 4, value: value }, () => this.getExplorer());
        break;
      case "Jardinage":
        this.setState({ categorie: 5, value: value }, () => this.getExplorer());
        break;

      default:
        this.setState({ categorie: 0, value: value }, () => this.getExplorer());
        break;
    }
  }

  renderItem(item, index) {
    return (
      <BoutiqueItem
        desc={item.desc}
        image={item.img}
        prix={item.prix}
        onPress={() => this.navigateToDetails(item.desc, item.prix, item.img)}
      />
    );
  }

  renderFiltre() {
    return (
      <View>
        <Menu style={{ marginRight: 6, marginTop: 6 }} onSelect={() => ({})}>
          <MenuTrigger>
            <View style={styles.btn}>
              <Text style={[styles.menuTxt, { color: Themes.defaultPrimaryColor }]}>{this.state.value}</Text>
              <Icon name="arrow-drop-down" size={24} color={Themes.defaultPrimaryColor} />
            </View>
          </MenuTrigger>

          <MenuOptions optionsContainerStyle={{ marginHorizontal: 16 }}>
            <MenuOption value={"Tout"} style={styles.options}>
              <Text style={styles.menuTxt}>Tout</Text>
            </MenuOption>
            <MenuOption value={"Entretien"} style={styles.options}>
              <Text style={styles.menuTxt}>Entretien</Text>
            </MenuOption>
            <MenuOption value={"Bébé"} style={styles.options}>
              <Text style={styles.menuTxt}>Bébé</Text>
            </MenuOption>
            <MenuOption value={"Cuisine"} style={styles.options}>
              <Text style={styles.menuTxt}>Cuisine</Text>
            </MenuOption>
            <MenuOption value={"Jardinage "} style={styles.options}>
              <Text style={styles.menuTxt}>Jardinage</Text>
            </MenuOption>
          </MenuOptions>
        </Menu>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.page}>
        <MenuContext>
          <FlatList
            ref={ref => {
              this.flatList = ref;
            }}
            ListHeaderComponent={
              <View>
                <SearchBar />
                <View style={styles.flitre}>
                  <Text style={[styles.menuTxt, { color: Themes.defaultPrimaryColor }]}>Afficher :</Text>
                  {this.renderFiltre()}
                </View>
              </View>
            }
            data={this.state.dataSource}
            keyExtractor={item => item.id}
            showsVerticalScrollIndicator={false}
            horizontal={false}
            numColumns={3}
            useNativeDriver={true}
            renderItem={({ item, index }) => this.renderItem(item, index)}
          />
        </MenuContext>
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#2c3e50",
  },

  page: {
    flex: 1,
    backgroundColor: Themes.appBackgroundColor,
  },
  txtWelcome: {
    fontSize: Themes.h4,
    margin: Themes.h4,
  },
  flitre: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: Themes.m6,
    backgroundColor: "white",
  },
  btn: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  options: {
    height: 44,
  },
  menuTxt: {
    color: "#000",
    fontSize: 15,
    marginLeft: 4,
    marginRight: 4,
  },
});

//make this component available to the app
export default Boutique;
