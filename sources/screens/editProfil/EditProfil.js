import React, { Component } from "react";
import { View, Text, StyleSheet, Dimensions, TouchableHighlight, Image, TextInput } from "react-native";
//import Icon from "react-native-vector-icons/Ionicons";
import Icon from "react-native-vector-icons/MaterialIcons";

import Themes from "../../commons/Themes";
const screenWidth = Dimensions.get("window").width;
const cellWidth = screenWidth / 3 - 4;
export default class EditProfil extends Component {
  static navigationOptions = {
    title: "Editer mon profil",
  };

  navigateToMonProfil() {
    let { navigate } = this.props.navigation;
    navigate("monProfil", {});
  }
  render() {
    return (
      <View style={styles.container}>
        <View>
          <TouchableHighlight style={styles.imgHolder} underlayColor="white" onPress={() => {}}>
            <Image style={styles.addSymbol} resizeMode="cover" />
          </TouchableHighlight>
        </View>
        <View style={styles.form}>
          <View style={{ paddingTop: Themes.m16, alignItems: "center" }}>
            <Text style={{ fontSize: 14, fontWeight: "bold" }}>Editez votre profil</Text>
          </View>
          <TextInput
            underlineColorAndroid={"transparent"}
            placeholder={"Nom"}
            placeholderTextColor={"#ccc"}
            style={styles.inpt}
          />
          <TextInput
            underlineColorAndroid={"transparent"}
            placeholder={"Prenom"}
            placeholderTextColor={"#ccc"}
            style={styles.inpt}
          />
          <View style={styles.inpt}>
            <View style={{ justifyContent: "center" }}>
              <Icon name="location-on" size={24} color={Themes.dividerColor} />
            </View>
            <TextInput
              style={{ justifyContent: "center", paddingLeft: 16, fontSize: 18 }}
              underlineColorAndroid={"transparent"}
              placeholder={"Adresse"}
              placeholderTextColor={"#ccc"}
            />
          </View>
          <View style={{ paddingVertical: Themes.m32, alignItems: "center" }}>
            <TouchableHighlight
              style={styles.btnList}
              underlayColor={Themes.ef}
              onPress={() => this.navigateToMonProfil()}>
              <View style={{ justifyContent: "center", alignItems: "center" }}>
                <Text style={{ color: "#FFF", fontSize: 18, fontWeight: "bold" }}>VALIDER</Text>
              </View>
            </TouchableHighlight>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Themes.appBackgroundColor,
  },
  imgHolder: {
    alignItems: "center",
    justifyContent: "center",
    width: cellWidth,
    height: cellWidth,
    backgroundColor: Themes.ef,
    borderRadius: cellWidth / 2,
    alignSelf: "center",
    marginTop: Themes.m20,
    elevation: Themes.m2,
  },
  addSymbol: {
    height: cellWidth,
    width: cellWidth,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    borderRadius: cellWidth / 2,
  },
  form: {
    margin: Themes.m16,
  },
  inpt: {
    flexDirection: "row",
    paddingLeft: 16,
    fontSize: 18,
    marginTop: 8,
    marginBottom: 8,
    borderWidth: 0.5,
    borderColor: Themes.dividerColor,
    backgroundColor: Themes.accentColor,
    borderRadius: 3,
  },
  btnList: {
    height: 70,
    width: 328,
    //borderWidth: 1,
    //borderColor: Themes.defaultPrimaryColor,
    borderRadius: 3,
    marginBottom: Themes.m16,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Themes.defaultPrimaryColor,
    elevation: 3,
  },
});
