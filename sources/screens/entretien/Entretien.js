import React, { Component } from "react";
import { View, Text, StyleSheet, Picker, TextInput, FlatList, TouchableHighlight } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

import Themes from "../../commons/Themes";

export default class Entretien extends Component {
  static navigationOptions = {
    title: "Agent d'entretien",
  };
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [{}, {}, {}, {}, {}],
      ville: "",
    };
  }

  _keyExtractor = (item, index) => item.id;

  _renderItem = ({ item }) => (
    <View style={styles.imgCont}>
      <TouchableHighlight underlayColor={Themes.transparent} onPress={() => this.navigateToHotel()}>
        <View style={{ flexDirection: "row" }}>
          <View style={styles.img}>
            <Text>image</Text>
          </View>

          <View style={styles.infoImgR}>
            <Text>BAGNA Farras</Text>
          </View>
        </View>
      </TouchableHighlight>

      <View style={styles.infoImgB}>
        <View style={{ justifyContent: "center" }}>
          <Icon name="location-on" size={24} color={Themes.dividerColor} />
        </View>
        <Text>Lomé: Agoe-Cacaveli</Text>
      </View>
    </View>
  );

  navigateToHotel() {
    let { navigate } = this.props.navigation;
    navigate("hotel", {});
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={{ backgroundColor: Themes.defaultPrimaryColor }}>
          <View style={styles.form}>
            <View style={styles.picker}>
              <Picker
                selectedValue={this.state.ville}
                onValueChange={(itemValue, itemIndex) => this.setState({ ville: itemValue })}
                style={{}}>
                <Picker.Item label="Lomé" value="lomé" />
                <Picker.Item label="Aneho" value="aneho" />
              </Picker>
            </View>
            <View>
              <TextInput
                underlineColorAndroid={"transparent"}
                placeholder={"Nom de l'agent"}
                // onChangeText={str => {
                //   this.setState({ codeScurite: str });
                //   G_codeScurite = str;
                // }}
                placeholderTextColor={"#ccc"}
                style={styles.inpt}
              />
            </View>
          </View>
        </View>
        <View style={styles.list}>
          <FlatList
            data={this.state.dataSource}
            extraData={this.state}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
          />
          <View />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Themes.appBackgroundColor,
  },
  form: {
    margin: Themes.m16,
  },
  picker: {
    paddingLeft: Themes.m8,
    height: 40,
    width: 200,
    borderWidth: 0.5,
    borderRadius: 3,
    borderColor: Themes.darkPrimaryColor,
    backgroundColor: Themes.accentColor,
  },
  inpt: {
    paddingLeft: 16,
    marginTop: 16,
    fontSize: 18,
    borderWidth: 0.5,
    borderColor: Themes.darkPrimaryColor,
    backgroundColor: Themes.accentColor,
    borderRadius: 3,
  },
  list: {
    flex: 1,
    backgroundColor: Themes.ef,
  },
  imgCont: {
    height: 160,
    marginVertical: Themes.m5,
    marginHorizontal: Themes.m10,
    borderRadius: 3,
    elevation: 2,
    backgroundColor: Themes.accentColor,
  },
  img: {
    alignItems: "center",
    justifyContent: "center",
    height: 110,
    width: 130,
    backgroundColor: Themes.ef,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
  },
  infoImgR: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: 100,
    borderTopRightRadius: 3,
    backgroundColor: Themes.accentColor,
  },
  infoImgB: {
    alignItems: "center",
    justifyContent: "center",
    height: 50,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    backgroundColor: Themes.accentColor,
    borderTopWidth: 1,
    borderColor: Themes.dd,
  },
});
