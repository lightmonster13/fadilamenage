import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";

import Themes from "../../commons/Themes";

export default class Client extends Component {
  static navigationOptions = {
    title: "Client",
  };
  render() {
    return (
      <View style={styles.container}>
        <Text> hi </Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Themes.appBackgroundColor,
  },
});
