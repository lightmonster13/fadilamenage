import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableHighlight, Dimensions, Image, TextInput } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import Themes from "../../commons/Themes";
import EditBtn from "../../commons/EditBtn";

const screenWidth = Dimensions.get("window").width;
const cellWidth = screenWidth / 3 - 4;
export default class MonProfil extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Mon Profil",
    headerRight: <EditBtn navigation={navigation} />,
  });
  navigateToEditProfil() {
    let { navigate } = this.props.navigation;
    navigate("editProfil", {});
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.imgHolder}>
          <Image style={styles.addSymbol} resizeMode="cover" />
        </View>
        <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center", marginTop: Themes.m16 }}>
          <View style={{ marginBottom: Themes.m6, alignItems: "center" }}>
            <Text style={{ fontSize: Themes.h4, fontStyle: "italic" }}>BAGNA Farras</Text>
            <Text style={{ fontSize: Themes.h6, fontStyle: "italic", fontWeight: "bold" }}>Chef</Text>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Themes.appBackgroundColor,
  },
  imgHolder: {
    alignItems: "center",
    justifyContent: "center",
    width: cellWidth,
    height: cellWidth,
    backgroundColor: Themes.ef,
    borderRadius: cellWidth / 2,
    alignSelf: "center",
    marginTop: Themes.m20,
    elevation: Themes.m2,
  },
  addSymbol: {
    height: cellWidth,
    width: cellWidth,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    borderRadius: cellWidth / 2,
  },
  form: {
    margin: Themes.m16,
  },
  inpt: {
    flexDirection: "row",
    paddingLeft: 16,
    fontSize: 18,
    marginTop: 8,
    marginBottom: 8,
    borderWidth: 0.5,
    borderColor: Themes.dividerColor,
    backgroundColor: Themes.accentColor,
    borderRadius: 3,
  },
  btnList: {
    height: 70,
    width: 328,
    //borderWidth: 1,
    //borderColor: Themes.defaultPrimaryColor,
    borderRadius: 3,
    marginBottom: Themes.m16,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Themes.defaultPrimaryColor,
    elevation: 3,
  },
});
