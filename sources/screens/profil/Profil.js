//import liraries
import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableHighlight } from "react-native";

import Themes from "../../commons/Themes";

// create a component
class Profil extends Component {
  static navigationOptions = {
    title: "Choix de profil",
  };
  navigateToClientProfil() {
    let { navigate } = this.props.navigation;
    navigate("clientProfil", {});
  }

  navigateToCertifier() {
    let { navigate } = this.props.navigation;
    navigate("certifier", {});
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={{ paddingBottom: Themes.m32, alignItems: "center" }}>
          <Text style={{ fontSize: 14, fontWeight: "bold" }}>Choisissez votre Profil</Text>
        </View>
        <TouchableHighlight
          style={styles.btnList}
          underlayColor={Themes.ef}
          onPress={() => this.navigateToClientProfil()}>
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <Text style={{ color: Themes.dividerColor, fontSize: 18, fontWeight: "bold" }}>CLIENT</Text>
          </View>
        </TouchableHighlight>

        <TouchableHighlight style={styles.btnList} underlayColor={Themes.ef} onPress={() => this.navigateToCertifier()}>
          <View>
            <Text style={{ color: Themes.dividerColor, fontSize: 18, fontWeight: "bold" }}> CERTIFIER </Text>
          </View>
        </TouchableHighlight>
        <View />
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Themes.whiteSmoke,
  },
  btnList: {
    height: 70,
    width: 328,
    borderWidth: 1,
    borderColor: Themes.defaultPrimaryColor,
    borderRadius: 3,
    marginBottom: Themes.m16,
    justifyContent: "center",
    alignItems: "center",
  },
});

//make this component available to the app
export default Profil;
