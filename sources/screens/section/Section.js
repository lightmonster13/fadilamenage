import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableHighlight, Image } from "react-native";
import Themes from "../../commons/Themes";

import { ScrollView } from "react-native-gesture-handler";

// create a component
class Section extends Component {
  static navigationOptions = {
    title: "Section",
  };
  navigateToEntretien() {
    let { navigate } = this.props.navigation;
    navigate("entretien", {});
  }
  navigateToNounous() {
    let { navigate } = this.props.navigation;
    navigate("nounous", {});
  }
  navigateToChef() {
    let { navigate } = this.props.navigation;
    navigate("chef", {});
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.slideContainer}>
          <Text>Pub des articles de la boutique</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <TouchableHighlight
            style={styles.btnList}
            underlayColor={Themes.ef}
            onPress={() => this.navigateToEntretien()}>
            <View style={{ flexDirection: "row", alignItems: "center", paddingHorizontal: Themes.m16 }}>
              <Image
                resizeMode={"contain"}
                style={{
                  height: 90,
                  width: 90,
                }}
                source={require("../../commons/assets/menage.png")}
              />
              <View style={{ paddingHorizontal: Themes.m16 }}>
                <Text style={{ fontSize: 14 }}> NETTOYAGE ET ENTRETIEN</Text>
              </View>
            </View>
          </TouchableHighlight>
          <TouchableHighlight style={styles.btnList} underlayColor={Themes.ef} onPress={() => this.navigateToChef()}>
            <View style={{ flexDirection: "row", alignItems: "center", paddingHorizontal: Themes.m16 }}>
              <Image
                resizeMode={"contain"}
                style={{
                  height: 90,
                  width: 90,
                }}
                source={require("../../commons/assets/Chefs.png")}
              />
              <View style={{ paddingHorizontal: Themes.m16 }}>
                <Text style={{ fontSize: 14 }}> CUISINIERS ET CHEFS</Text>
              </View>
            </View>
          </TouchableHighlight>
          <TouchableHighlight style={styles.btnList} underlayColor={Themes.ef} onPress={() => this.navigateToNounous()}>
            <View style={{ flexDirection: "row", alignItems: "center", paddingHorizontal: Themes.m16 }}>
              <Image
                resizeMode={"contain"}
                style={{
                  height: 90,
                  width: 90,
                }}
                source={require("../../commons/assets/nounous.png")}
              />
              <View style={{ paddingHorizontal: Themes.m16 }}>
                <Text style={{ fontSize: 14 }}> NOUNOUS</Text>
              </View>
            </View>
          </TouchableHighlight>
          <TouchableHighlight style={styles.btnList} underlayColor={Themes.ef} onPress={() => {}}>
            <View style={{ flexDirection: "row", alignItems: "center", paddingHorizontal: Themes.m16 }}>
              <Image
                resizeMode={"contain"}
                style={{
                  height: 90,
                  width: 90,
                }}
                source={require("../../commons/assets/jardinier.png")}
              />
              <View style={{ paddingHorizontal: Themes.m16 }}>
                <Text style={{ fontSize: 14 }}> JARDINAGE</Text>
              </View>
            </View>
          </TouchableHighlight>
        </ScrollView>
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Themes.whiteSmoke,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: Themes.m16,
  },
  slideContainer: {
    width: 328,
    height: 140,
    backgroundColor: Themes.dd,
    marginBottom: Themes.m16,
    marginTop: Themes.m16,
    justifyContent: "center",
    alignItems: "center",
  },
  btnList: {
    height: 100,
    width: 328,
    borderWidth: 1,
    borderColor: Themes.dividerColor,
    borderRadius: 3,
    marginBottom: Themes.m16,
    justifyContent: "center",
    //alignItems: "center",
  },
});

//make this component available to the app
export default Section;
