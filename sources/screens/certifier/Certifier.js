import React, { Component } from "react";
import { View, Text, StyleSheet, Picker, TextInput, TouchableHighlight } from "react-native";

import Themes from "../../commons/Themes";

export default class Certifier extends Component {
  static navigationOptions = {
    title: "Certifier",
  };
  constructor(props) {
    super(props);
    this.state = {
      domaine: "",
    };
  }
  navigateToMonProfil() {
    let { navigate } = this.props.navigation;
    navigate("monProfil", {});
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={{ paddingTop: Themes.m16, alignItems: "center" }}>
          <Text style={{ fontSize: 14, fontWeight: "bold" }}>Choisissez votre domaine</Text>
        </View>

        <View style={styles.form}>
          <View style={styles.picker}>
            <Picker
              selectedValue={this.state.ville}
              onValueChange={(itemValue, itemIndex) => this.setState({ ville: itemValue })}
              style={{}}>
              <Picker.Item label="Agent de ménage" value="ménage" />
              <Picker.Item label="Chef cuisinier" value="chef" />
              <Picker.Item label="Nounous" value="nounous" />
              <Picker.Item label="Jardinier" value="jardinier" />
            </Picker>
          </View>
          <View>
            <View style={{ paddingTop: Themes.m16, alignItems: "center" }}>
              <Text style={{ fontSize: 14, fontWeight: "bold" }}>Entrez votre code de certification</Text>
            </View>
            <TextInput
              underlineColorAndroid={"transparent"}
              placeholder={"Code de certification"}
              placeholderTextColor={"#ccc"}
              style={styles.inpt}
            />

            <TextInput
              underlineColorAndroid={"transparent"}
              placeholder={"+ 228 91 14 98 49"}
              editable={false}
              placeholderTextColor={"#ccc"}
              style={styles.inpt}
            />
          </View>
        </View>
        <View style={{ paddingVertical: Themes.m32, alignItems: "center" }}>
          <TouchableHighlight
            style={styles.btnList}
            underlayColor={Themes.ef}
            onPress={() => this.navigateToMonProfil()}>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <Text style={{ color: "#FFF", fontSize: 18, fontWeight: "bold" }}>SOUMETTRE</Text>
            </View>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Themes.appBackgroundColor,
  },
  form: {
    margin: Themes.m16,
  },
  picker: {
    paddingLeft: Themes.m8,
    height: 40,
    width: 200,
    borderWidth: 0.5,
    borderRadius: 3,
    borderColor: Themes.dividerColor,
    backgroundColor: Themes.accentColor,
  },
  inpt: {
    paddingLeft: 16,
    marginTop: 16,
    marginBottom: 16,
    fontSize: 18,
    borderWidth: 0.5,
    borderColor: Themes.dividerColor,
    backgroundColor: Themes.accentColor,
    borderRadius: 3,
  },
  btnList: {
    height: 70,
    width: 328,
    //borderWidth: 1,
    //borderColor: Themes.defaultPrimaryColor,
    borderRadius: 3,
    marginBottom: Themes.m16,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Themes.defaultPrimaryColor,
    elevation: 3,
  },
});
