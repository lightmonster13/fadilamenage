import React from "react";
import { View, Text } from "react-native";

import ROUTES from "./routes/index";

export default class Main extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <ROUTES />
      </View>
    );
  }
}
